<?php

namespace Lexik\Bundle\TopOrFlopBundle\Controller;

use Lexik\Bundle\TopOrFlopBundle\Entity\Media;
use Lexik\Bundle\TopOrFlopBundle\Form\MediaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class BackendController
 *
 * @package Lexik\Bundle\TopOrFlopBundle\Controller
 *
 * @Route("/backend")
 */
class BackendController extends Controller
{
    /**
     * @Route("/", name="backend_new")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request)
    {
        $media = new Media();

        $form = $this->createForm(new MediaType(), $media);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($media);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_new'));
        }

        return $this->render('LexikTopOrFlopBundle:Backend:new.html.twig', array(
            'form'  => $form->createView(),
        ));
    }
}
