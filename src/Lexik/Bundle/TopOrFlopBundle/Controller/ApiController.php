<?php

namespace Lexik\Bundle\TopOrFlopBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class ApiController
 *
 * @package Lexik\Bundle\TopOrFlopBundle\Controller
 */
class ApiController extends Controller
{
    /**
     * @Route(
     *     "/api/medias.{_format}",
     *     name="api_media",
     *     requirements={"_format"= "json|xml"},
     *     defaults={"_format" = "json"}
     * )
     * @Method({"GET"})
     *
     * @param string $_format
     *
     * @return Response
     */
    public function getMediasAction($_format = 'json')
    {
        $medias = $this->getDoctrine()
            ->getRepository('Lexik\Bundle\TopOrFlopBundle\Entity\Media')
            ->findAll()
        ;

        $serializer = $this->get('jms_serializer');
        $data = $serializer->serialize($medias, $_format);

        return new Response($data, 200);
    }
}
