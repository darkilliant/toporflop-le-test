<?php

namespace Lexik\Bundle\TopOrFlopBundle\Service;

use Doctrine\ORM\EntityManager;

use Symfony\Component\Security\Core\SecurityContext;

use Lexik\Bundle\TopOrFlopBundle\Entity\User;
use Lexik\Bundle\TopOrFlopBundle\Entity\Media;
use Lexik\Bundle\TopOrFlopBundle\Entity\Vote;

/**
 * Class MediaManager
 *
 * @package Lexik\Bundle\TopOrFlopBundle\Service
 */
class MediaManager
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var SecurityContext
     */
    private $context;

    /**
     * Constructor
     *
     * @param EntityManager   $entityManager
     * @param SecurityContext $securityContext
     */
    public function __construct(EntityManager $entityManager, SecurityContext $securityContext)
    {
        $this->em = $entityManager;
        $this->context = $securityContext;
    }

    /**
     * @return User|null
     */
    protected function getUser()
    {
        if (null === $this->context || null === $token = $this->context->getToken()) {
            return null;
        }

        $user = $token->getUser();
        if ($user instanceof User) {
            return $user;
        }

        return null;
    }

    /**
     * Get a media object from an id, with votes hydrated
     *
     * @param  integer $id
     * @return Media
     */
    public function getMedia($id)
    {
        $mediaRepository = $this->em->getRepository('Lexik\Bundle\TopOrFlopBundle\Entity\Media');
        $media = $mediaRepository->getHydratedMediaById($id);

        return $media instanceof Media ? $media : null;
    }

    /**
     * Get the next media to display depending on the user
     *
     * @return Media
     */
    public function getNextMedia()
    {
        $user = $this->getUser();
        $mediaRepository = $this->em->getRepository('Lexik\Bundle\TopOrFlopBundle\Entity\Media');

        // user authenticated
        if ($user instanceof User) {
            $media = $mediaRepository->getNewMediaForUser($user);

            // found a media the user hasn't voted for yet
            if ($media instanceof Media) {
                return $media;
            }
        }

        // default behavior: just get a random media
        return $mediaRepository->getRandomMedia();
    }

    /**
     * Get a new vote object for current user and given media
     *
     * @param Media $media
     *
     * @return Vote
     */
    public function getNewVote(Media $media)
    {
        $user = $this->getUser();

        if (!$user instanceof User) {
            return null;
        }

        $vote = new Vote();
        $vote->setUser($user);
        $vote->setMedia($media);

        return $vote;
    }
}
